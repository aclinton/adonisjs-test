'use strict';

/** @type {require('../../Services/Mojang/Mojang')} */
const Mojang = use('Adonisjs/Services/Mojang');

class MojangController {
  async show({request, response}) {
    let res = await Mojang.username().find(request.params.id);
    let d = {data: null};

    if (res.wasSuccessful()) {
      d.data = res.username();
    }

    response.json(d);
  }
}

module.exports = MojangController;
