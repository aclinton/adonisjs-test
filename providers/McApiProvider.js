'use strict';

const {ServiceProvider} = require('@adonisjs/fold');
import {Client, McApi} from "../build/Services/McApi";

class McApiProvider extends ServiceProvider {
  register() {
    this.app.singleton('App/Services/McApi', () => {
      return new McApi(new Client);
    });
  }
}
