'use strict';

const {ServiceProvider} = require('@adonisjs/fold');
const {Mojang, Client} = require('../build/Services/Mojang');

class MojangProvider extends ServiceProvider {
  register() {
    this.app.singleton('Adonisjs/Services/Mojang', () => {
      return new Mojang(new Client());
    });
  }
}

module.exports = MojangProvider;
