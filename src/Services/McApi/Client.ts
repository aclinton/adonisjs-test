import axios, {AxiosInstance} from 'axios';

class Client {
  protected client: AxiosInstance;

  constructor() {
    this.client = axios.create({baseURL: 'https://mcapi.us/server/'});
  }

  get(url: string, params: object) {
    return this.client({
      'method': 'get',
      'url': url,
      'params': params
    });
  }
}

export {Client};
