export default class Players {
  max: number;
  now: number;
  list: [];

  constructor(json?: object) {
    if(json !== null) {
      this._setFields(json);
    }
  }

  private _setFields(json: object): void {
    if(json.hasOwnProperty('max')) {
      this.max = json['max'];
    }

    if(json.hasOwnProperty('now')) {
      this.now = json['now'];
    }

    if(json.hasOwnProperty('list')) {
      this.list = json['list'];
    }
  }
}
