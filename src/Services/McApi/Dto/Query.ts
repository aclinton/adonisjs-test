import Players from "./Players";

export default class Query {
  status: string;
  error: string;
  motd: string;
  version: string;
  gameType: string;
  gameId: string;
  serverMod: string;
  map: string;
  lastOnline: string;
  lastUpdated: string;
  duration: string;
  online: boolean;
  players: Players;

  constructor(json?: object) {
    if (json !== null) {
      this._setFields(json);
    }
  }

  private _setFields(json: object): void {
    if (json.hasOwnProperty('status')) {
      this.status = json['status'];
    }

    if (json.hasOwnProperty('error')) {
      this.error = json['error'];
    }

    if (json.hasOwnProperty('motd')) {
      this.motd = json['motd'];
    }

    if (json.hasOwnProperty('version')) {
      this.version = json['version'];
    }

    if (json.hasOwnProperty('gameType')) {
      this.gameType = json['gameType'];
    }

    if (json.hasOwnProperty('gameId')) {
      this.gameId = json['gameId'];
    }

    if (json.hasOwnProperty('serverMod')) {
      this.serverMod = json['serverMod'];
    }

    if (json.hasOwnProperty('map')) {
      this.map = json['map'];
    }

    if (json.hasOwnProperty('lastOnline')) {
      this.lastOnline = json['lastOnline'];
    }

    if (json.hasOwnProperty('lastUpdated')) {
      this.lastUpdated = json['lastUpdated'];
    }

    if (json.hasOwnProperty('duration')) {
      this.duration = json['duration'];
    }

    if (json.hasOwnProperty('online')) {
      this.online = json['online'];
    }

    if(json.hasOwnProperty('players')) {
      this.players = new Players(json['players']);
    }
  }
}
