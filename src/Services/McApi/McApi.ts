import {Client} from "./Client";
import QueryParameters from "./Parameters/QueryParameters";
import QueryRequest from "./Requests/QueryRequest";

class McApi {
  protected client: Client;

  constructor(client: Client) {
    this.client = client;
  }

  query(): QueryRequest {
    return new QueryRequest(this.client);
  }

  queryParams(): QueryParameters {
    return new QueryParameters();
  }
}

export {McApi};
