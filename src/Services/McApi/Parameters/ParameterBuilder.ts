export default abstract class ParameterBuilder {
  protected parameters: [];

  /**
   * @param param
   * @param value
   */
  setParameter(param: string, value: any): void {
    this.parameters[param] = value;
  }

  getParameters(): [] {
    return this.parameters;
  }
}
