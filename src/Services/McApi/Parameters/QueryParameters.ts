import ParameterBuilder from "./ParameterBuilder";

export default class QueryParameters extends ParameterBuilder {
  ip(ip: string = ''): QueryParameters {
    this.setParameter('ip', ip);
    return this;
  }

  port(port: string = ''): QueryParameters {
    this.setParameter('port', port);

    return this;
  }
}
