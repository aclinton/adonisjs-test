import BaseRequest from "./BaseRequest";
import QueryParameters from "../Parameters/QueryParameters";
import QueryResponse from "../Responses/QueryResponse";

export default class QueryRequest extends BaseRequest {
  async all(parameters?: QueryParameters) {
    return (new QueryResponse()).getResult(this.client.get('query', parameters ? parameters : {}))
  }
}
