import Response from "./Response";
import Query from "../Dto/Query";

export default class QueryResponse extends Response {
  query(): Query {
    return this.data;
  }

  convertBody(): void {
    this.data = new Query(this.encodedBody());
  }
}
