import {Client} from "../Client";
import {AxiosPromise} from "axios";

export default abstract class Response {
  private body: any;
  private status: any;
  private error: any;
  protected data: any;

  async getResult(promise: AxiosPromise) {
    try {
      let res = await promise;
      this.status = res['status'];
      this.body = res['data'];
      this.data = this.body;
    } catch (error) {
      this.error = error;
    }

    return this;
  }

  abstract convertBody(): void;

  wasSuccessful(): boolean {
    return this.status > 199 && this.status < 300;
  }

  getStatus(): number {
    return this.status;
  }

  rawBody() {
    return this.body;
  }

  encodedBody() {
    return this.body;
  }
}
