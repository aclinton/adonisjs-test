"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var axios_1 = require("axios");
var Client = /** @class */ (function () {
    function Client() {
        this.client = axios_1.default.create({ baseURL: 'https://api.mojang.com/users/profiles/minecraft/' });
    }
    Client.prototype.getRequest = function (url, parameters) {
        return this.client.get(url, {
            params: parameters
        });
    };
    return Client;
}());
exports.default = Client;
