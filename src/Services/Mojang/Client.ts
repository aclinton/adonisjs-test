import axios, {AxiosInstance} from 'axios';

class Client {
  protected client: AxiosInstance;

  constructor() {
    this.client = axios.create({baseURL: 'https://api.mojang.com/users/profiles/minecraft/'});
  }

  getRequest(url: string, parameters?: any) {
    return this.client.get(url, {
      params: parameters
    });
  }
}

export {Client};
