export default class Username {
  id: any;
  name: string;

  constructor(json?: object) {
    if(json !== null) {
      this._setFields(json);
    }
  }

  /**
   * @param json
   * @private
   */
  private _setFields(json: object): void {
    if(json.hasOwnProperty('id')) {
      this.id = json['id'];
    }

    if(json.hasOwnProperty('name')) {
      this.name = json['name'];
    }
  }
}
