"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var UsernameRequest_1 = require("./Requests/UsernameRequest");
var Mojang = /** @class */ (function () {
    function Mojang(client) {
        this.client = client;
    }
    Mojang.prototype.username = function () {
        return new UsernameRequest_1.default(this.client);
    };
    return Mojang;
}());
exports.default = Mojang;
