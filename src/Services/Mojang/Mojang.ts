import UsernameRequest from './Requests/UsernameRequest';
import {Client} from './Client';

class Mojang {
  protected client: Client;

  constructor(client: Client) {
    this.client = client;
  }

  username(): UsernameRequest {
    return new UsernameRequest(this.client);
  }
}

export {
  Mojang
}
