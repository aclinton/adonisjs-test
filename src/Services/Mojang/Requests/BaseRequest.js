"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var BaseRequest = /** @class */ (function () {
    /**
     *  Constructor
     * @param client
     */
    function BaseRequest(client) {
        if (this.constructor === BaseRequest) {
            throw new TypeError('Abstract class "BaseRequest" cannot be instantiated directly.');
        }
        this.client = client;
    }
    return BaseRequest;
}());
exports.default = BaseRequest;
