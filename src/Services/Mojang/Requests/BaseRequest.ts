import {Client} from '../Client';

export default abstract class BaseRequest {
  protected client: Client;

  /**
   *  Constructor
   * @param client
   */
  constructor(client: Client) {
    this.client = client;
  }
}
