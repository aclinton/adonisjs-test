import UsernameResponse from '../Responses/SingleUsernameResponse';
import BaseRequest from './BaseRequest';

export default class UsernameRequest extends BaseRequest {
  async find(username: string) {
    return (new UsernameResponse(this.client)).response(username);
  }
}
