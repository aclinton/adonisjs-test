import {Client} from  '../Client';
import UserName from '../Dto/Username';

export default class SingleUsernameResponse {
  protected data: any;
  protected error: any;
  protected client: Client;
  protected status_code: number | null;

  constructor(client: Client) {
    this.data = null;
    this.error = null;
    this.client = client;
    this.status_code = null;
  }

  username() {
    return this.getData();
  }

  /**
   * @param url
   */
  async response(url) {
    try {
      let res = await this.client.getRequest(url);
      this.status_code = res.status;

      this.parseResource(res.data);
    } catch (error) {
      this.error = error;
    }

    return this;
  }

  wasSuccessful(): boolean {
    return this.status_code > 199 && this.status_code < 300;
  }

  getData() {
    return this.data;
  }

  protected parseResource(json): void {
    this.data = new UserName(json);
  }
}

